package org.sti2.triplecounter;

import java.io.File;
import java.io.FileNotFoundException;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

public class Main {

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		 System.out.println(args[0]);
		 //System.setProperty("com.github.jsonldjava.disallowRemoteContextLoading", "true");
		 Model model = ModelFactory.createDefaultModel();		 
		 long count = StatementCounter.countStatements2(model, new File(args[0]));
		 
		 System.out.println(count);

	}

}
