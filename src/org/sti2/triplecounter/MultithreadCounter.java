package org.sti2.triplecounter;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.sparql.JenaTransactionException;

import com.github.jsonldjava.core.DocumentLoader;
import com.github.jsonldjava.core.JsonLdConsts;
import com.github.jsonldjava.core.JsonLdError;
import com.github.jsonldjava.core.JsonLdProcessor;
import com.github.jsonldjava.core.RDFDataset;
import com.github.jsonldjava.core.RDFDataset.Quad;
import com.github.jsonldjava.impl.TurtleTripleCallback;
import com.github.jsonldjava.utils.JsonUtils;

public class MultithreadCounter implements Runnable{

	public File file;
	
	public MultithreadCounter(File file) {
		this.file= file;
		 
	}
	
	@Override
	public void run() {
		long start = System.currentTimeMillis();
		Model model = ModelFactory.createDefaultModel();
		String base = file.getAbsolutePath();
		
		//System.setProperty(DocumentLoader.DISALLOW_REMOTE_CONTEXT_LOADING, "true");
		
		String jsonld = null;
		try {
			jsonld =FileUtils.readFileToString(new File(base), Charset.defaultCharset());
			RDFDataset dataset = new RDFDataset();
			
			List<Quad> quads = ((RDFDataset)JsonLdProcessor.toRDF(JsonUtils.fromString(jsonld))).getQuads(JsonLdConsts.DEFAULT);
			
			
			//RDFDataMgr.read(model, new ByteArrayInputStream(jsonld.getBytes()), "http://schema.org/",Lang.JSONLD);
			StatementCounter.count += quads.size();
			long end = System.currentTimeMillis();
			System.out.println(base+"...done in " + (end-start)/1000 + " seconds. "+ quads.size() + " triples." );
		} catch (FileNotFoundException e) {
			System.err.println(base);
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println(base);
			e.printStackTrace();
		}catch (Exception ex)
		{
			System.err.println(base);
			System.err.println(ex.getMessage());
			
		}
		
		
		
		

		
	}

}
