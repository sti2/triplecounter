package org.sti2.triplecounter;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public  class StatementCounter  {

	public static long count = 0;
	

	//	public static long countStatements(Model model, File folder) {
	//
	//		 String lang = "JSONLD";		 
	//		 model.removeAll();
	//		 Gson gson = new Gson();
	//		 
	//		 List<File> files = new ArrayList<File>();		 
	//		 listFiles(folder, files);
	//		 
	//		 for (File file:files) {
	//			 String base = file.getAbsolutePath();
	//			 try {
	//				 FileReader fread = new FileReader(file);
	//				 
	//				JsonElement elem = gson .fromJson(fread, JsonElement.class);
	//				 				 
	//				 if (elem instanceof JsonArray) {					 
	//					 JsonArray jar = elem.getAsJsonArray();
	//					 
	//					 for (int i=0; i<jar.size(); i++) {
	//						 JsonElement jel = jar.get(i);
	//						 
	//						 if (!jel.isJsonObject()) continue;
	//						 
	//						 JsonObject job = jel.getAsJsonObject();
	//						 String json = gson.toJson(job);						 
	//						 model.read(new StringReader(json), base, lang);
	//					 }
	//				 } else {
	//					 JsonObject job = elem.getAsJsonObject();
	//					 String json = gson.toJson(job);
	//					 
	//					 model.read(new StringReader(json), base, lang);
	//				 }
	//			 } catch (Exception ex) {
	//				 ex.printStackTrace();
	//				 System.out.println("Error : " + base);
	//			 }
	//		 }
	//		 
	//		 return model.size();
	//	 }

	public static long countStatements2 (Model model, File folder) throws FileNotFoundException
	{
		List<File> files = new ArrayList<File>();
		Utils.listFiles(folder, files);
		
		//files.add(new File("D:\\Projects\\arosa\\arosa\\en\\childrensskischool-juniorclub.json"));
		System.out.println("File count:"+files.size());
		ArrayList<Thread> threads = new ArrayList<Thread>();
		for (File file : files)
		{

			Thread t = new Thread(new MultithreadCounter(file));
			threads.add(t);
			t.start();
			
		}
		
        int running = 0;
        int last = 0;
        do {
            running = 0;
            ArrayList<Thread> deads = new ArrayList<Thread>();
            for (Thread thread : threads) {
                if (thread.isAlive()) {
                    running++;
                }else
                {
                	
                	deads.add(thread);
                	thread = null;
                }
            }
            if (last != (threads.size()-deads.size()))
            {
            	System.out.println("Remaining: " + (threads.size()-deads.size()));
            	last = (threads.size()-deads.size());
            }
            threads.removeAll(deads);
            //System.out.println("Remaining threads: " + running);
            
        } while (running > 0);

		//model.write(System.out,"NTRIPLES");
		return count;

	}






}



