package org.sti2.triplecounter;

import java.io.File;
import java.util.List;

public class Utils {
	public static void listFiles(File folder, List<File> list) {
		File[] files = folder.listFiles();
		if (files == null) return;
		
		for(int j = 0; j < files.length; j++) {
			if(files[j].isFile()) list.add(files[j]);
			if(files[j].isDirectory()) listFiles(files[j], list);
		}
	}
}
